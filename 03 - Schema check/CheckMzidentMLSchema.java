  .
  .
  .
  
  /**
   * This method validates an input mzIdentML file according to the supplied schema, and returns the outcome.
   *
   * @param schemaLocation the location of the schema
   * @param mzIdentML the input mzIdentML file.
   * @return a SchemaCheckResult - if the mzIdentML passed validation, validAgainstSchema will be true. False otherwise, and contains a list of the error messages.
   */
  private static SchemaCheckResult validateMzidSchema(String schemaLocation, File mzIdentML) {
    log.info("Validating mzIdentML XML schema for: " + mzIdentML.getPath() + " using schema: " + schemaLocation);
    SchemaCheckResult result = new SchemaCheckResult(false, new ArrayList<>());
    ValidationErrorHandler handler = new ValidationErrorHandler();
    try (BufferedReader br = new BufferedReader(new FileReader(mzIdentML))) {
      GenericSchemaValidator genericValidator = new GenericSchemaValidator();
      genericValidator.setSchema(new URI(schemaLocation));
      genericValidator.setErrorHandler(handler);
      genericValidator.validate(br);
      log.info(SCHEMA_OK_MESSAGE + mzIdentML.getName());
      List<String> errorMessages = handler.getErrorMessages();
      result.setValidAgainstSchema(errorMessages.size()<1);
      result.setErrorMessages(errorMessages);
    } catch (IOException | SAXException e) {
      log.error("Problem reading or parsing the file: ", e);
    } catch (URISyntaxException usi) {
      log.error("Unable to parse URI syntax: ", usi);
    }
    return result;
  }
  
  .
  .
  .