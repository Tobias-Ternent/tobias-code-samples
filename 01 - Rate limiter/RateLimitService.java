package uk.ac.ebi.pride.archive.web.service.interceptor;

import redis.clients.jedis.JedisPool;

/** This defines the interface for limiting the rate of users' requests.
 */
public interface RateLimitService {

  public int incrementLimit(String userKey, JedisPool jedisPool) throws Exception;
}
