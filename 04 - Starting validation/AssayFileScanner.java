  .
  .
  .  
  /**
   * Scans a submission.
   * @param submission the submission to scan
   * @return assay file summaries
   * @throws IOException problems scanning assay files of a submission
   */
  public Collection<AssayFileSummary> scan(Submission submission) throws IOException {
    ArrayList<AssayFileSummary> assayFileSummaries = new ArrayList<>();
    SubmissionType submissionType = submission.getProjectMetaData().getSubmissionType();
    if (submissionType.equals(SubmissionType.COMPLETE) || submissionType.equals(SubmissionType.PRIDE)) {
      List<DataFile> dataFiles = submission.getDataFiles();
      for (DataFile dataFile : dataFiles) {
        if (dataFile.isFile() && dataFile.getFileType().equals(ProjectFileType.RESULT)) {
          File actualAssayFile = fileFinder.find(dataFile.getFile());
          logger.info("Generating assay file summary on " + actualAssayFile.getAbsolutePath());
          AssayFileSummary assayFileSummary = scan(dataFile);
          assayFileSummaries.add(assayFileSummary);
        }
      }
    }
    return assayFileSummaries;
  }
  .
  .
  .
  /**
   * Scans a submission.
   * @param submission the submission to scan
   * @return assay file summaries
   * @throws IOException problems scanning assay files of a submission
   */
  public Collection<AssayFileSummary> scan(Submission submission) throws IOException {
    ArrayList<AssayFileSummary> assayFileSummaries = new ArrayList<>();
    SubmissionType submissionType = submission.getProjectMetaData().getSubmissionType();
    if (submissionType.equals(SubmissionType.COMPLETE) || submissionType.equals(SubmissionType.PRIDE)) {
      List<DataFile> dataFiles = submission.getDataFiles();
      for (DataFile dataFile : dataFiles) {
        if (dataFile.isFile() && dataFile.getFileType().equals(ProjectFileType.RESULT)) {
          File actualAssayFile = fileFinder.find(dataFile.getFile());
          logger.info("Generating assay file summary on " + actualAssayFile.getAbsolutePath());
          AssayFileSummary assayFileSummary = scan(dataFile);
          assayFileSummaries.add(assayFileSummary);
        }
      }
    }
    return assayFileSummaries;
  }
  .
  .
  .
  /**
   * Scans a particular data (assay) file.
   * @param dataFile the data (assay) file to scan
   * @return the assay file's summary
   * @throws IOException problems scannign the assay file
   */
  public AssayFileSummary scan(DataFile dataFile) throws IOException {
    AssayFileSummary fileSummary = new AssayFileSummary();
    File file = fileFinder.find(dataFile.getFile());
    logger.info("Scanning for metadata on assay file: " + file.getAbsolutePath());
    List<File> peakListFiles = getPeakFiles(dataFile);
    long tempSizeRequest = getTotalTempSizeMb(file, peakListFiles);
    gatherInitialMetadata(dataFile, fileSummary);
    try {
      List<String> peakFilesString = peakListFiles.stream().map(File::getAbsolutePath).collect(Collectors.toList());
      File ticketDir = file.getParentFile().getParentFile();
      logger.info("Ticket: " + ticketDir.getPath());
      final String TICKET_AND_ID = ticketDir.getName() + "_" + fileSummary.getId();
      final String TICKET_AND_ID_ERROR = TICKET_AND_ID + ERROR_SUFFIX;
      File pgLogDirectory = new File(validatorDirectory + "log/" + ticketDir.getName());
      if (!pgLogDirectory.exists()) {
        logger.info("Creating logging directory for: " + pgLogDirectory + " " + pgLogDirectory.mkdirs());
      }
      File outputReportFile = new File(pgLogDirectory.getPath() + "/" + TICKET_AND_ID + VALIDATION_REPORT_SUFFIX);
      File serializedSummary = new File(pgLogDirectory.getPath() + "/" + TICKET_AND_ID + VALIDATION_REPORT_SERIALIZED_SUFFIX);
      File lsfJobLog = new File(pgLogDirectory.getPath() + "/" + TICKET_AND_ID + "_validation.report.log");
      if (outputReportFile.exists()) {
        logger.info("Report file already exists, deleting it: " + (outputReportFile.delete() ? "DONE" : "Error when deleting"));
        if (serializedSummary.exists()) {
          logger.info("Serialized summary file already exists, deleting it: " + (serializedSummary.delete() ? "DONE" : "Error when deleting"));
        }
      }
      launchJobAndSubscribe(dataFile, fileSummary, file, tempSizeRequest, peakFilesString, TICKET_AND_ID, TICKET_AND_ID_ERROR, outputReportFile, serializedSummary, lsfJobLog);
    } catch (InterruptedException ie) {
      logger.error("Interrupted exception during assay validation: ", ie);
    }
    return fileSummary;
  }
  .
  .
  .
  /**
   * Launches a validation job on the LSF, and subscribes to the Redis channel awaiting response.
   * @param dataFile the data (assay) file to validate
   * @param fileSummary the file summary
   * @param file the (assay) file to validate
   * @param tempSizeRequest size of temporary space to request
   * @param peakFilesString the related peak files
   * @param TICKET_AND_ID the OK message to listen for, base off the ticket and ID
   * @param TICKET_AND_ID_ERROR   the erro rmessage to listen for, base off the ticket and ID plus error suffix
   * @param outputReportFile the output report file
   * @param serializedSummary the serialized summary file
   * @param lsfJobLog the log file for the LSF job
   * @throws IOException problems reading/writing files
   * @throws InterruptedException problems subscribing to Redis
   */
  private void launchJobAndSubscribe(DataFile dataFile, AssayFileSummary fileSummary, File file, long tempSizeRequest, List<String> peakFilesString, String TICKET_AND_ID, String TICKET_AND_ID_ERROR, File outputReportFile, File serializedSummary, File lsfJobLog) throws IOException, InterruptedException {
    Process p = getProcess(file, peakFilesString, TICKET_AND_ID, TICKET_AND_ID_ERROR, outputReportFile, lsfJobLog, tempSizeRequest);
    InputStream inputStream;
    BufferedReader bufferedReader;
    String line;
    String lsfJobID = "";
    String lsfJobIDerr = "";
    if (p != null) {
      p.waitFor();
      inputStream = p.getInputStream();
      bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
      while ((line = bufferedReader.readLine()) != null) {
        logger.info(line);
        if (line.contains("Job") && lsfJobID.isEmpty()) {
          lsfJobID = line.substring(line.indexOf('<') + 1, line.indexOf('>'));
          logger.info("Validation assay job id is: " + lsfJobID);
        } else if (line.contains("Job") && !lsfJobID.isEmpty()) {
          lsfJobIDerr = line.substring(line.indexOf('<') + 1, line.indexOf('>'));
          logger.info("Validation assay post error message job id is: " + lsfJobIDerr);
        }
      }
      bufferedReader.close();
      inputStream.close();
      if (p.exitValue() != 0) {
        InputStream errInputStream = p.getErrorStream();
        String msg = "Failed to copy file to launch mzid assay validation";
        logger.error(msg);
        bufferedReader = new BufferedReader(new InputStreamReader(errInputStream));
        while ((line = bufferedReader.readLine()) != null) {
          logger.error(line);
        }
        inputStream.close();
        bufferedReader.close();
        throw new IllegalStateException(msg);
      }
    }
    boolean foundReportFile = false;
    JedisPubSub jedisPubSub = setupSubscriber(TICKET_AND_ID, TICKET_AND_ID_ERROR, messageReceivedLatch, messageContainer,
        jedisServer, jedisPort, jedisPassword, jedisChannel);
    messageReceivedLatch.await();
    final String MESSAGE_RECEIVED = messageContainer.iterator().next();
    logger.info("Received REDIS message: " + MESSAGE_RECEIVED);
    jedisPubSub.unsubscribe();
    if (MESSAGE_RECEIVED.equalsIgnoreCase(TICKET_AND_ID)) {
      foundReportFile = killBackupJob(outputReportFile, lsfJobID, lsfJobIDerr);
    } else if (MESSAGE_RECEIVED.equalsIgnoreCase(TICKET_AND_ID_ERROR)) {
      logger.error("Job " + lsfJobID + " did not exit properly, see: " + lsfJobLog.getAbsolutePath());
    }
    // todo? still map the summary file, even in an "error" and flag these errors on the validation report email in HTML format
    if (foundReportFile) {
      findAndMapSummary(dataFile, fileSummary, outputReportFile, serializedSummary, lsfJobID);
    }
  }
  .
  .
  .
  /**
   * Calculates the total amount of temporary space required.
   * @param file the assay file to validate
   * @param peakListFiles the related peak files
   * @return the total amount of temporary space required (in MB).
   */
  private long getTotalTempSizeMb(File file, List<File> peakListFiles) {
    long result = (file.length()/1024)/1024;
    if (CollectionUtils.isNotEmpty(peakListFiles)) {
      for (File peakFile : peakListFiles) {
        result += (peakFile.length()/1024)/1024;
      }
    }
    result += 250; // add extra temp space buffer
    logger.info("Temp space to request in MB: " + result);
    return result;
  }
  .
  .
  .
  /**
   * Gets all the related peak files for a data (assay) file
   * @param dataFile the data (assay) file
   * @return a list of all the related peak files
   * @throws IOException problems reading peak files
   */
  private List<File> getPeakFiles(DataFile dataFile) throws IOException {
    MassSpecFileFormat fileFormat = MassSpecFileFormat.checkFormat(dataFile.getFile());
    List<File> peakListFiles = new ArrayList<>();
    if (MassSpecFileFormat.PRIDE.equals(fileFormat)) {
      logger.info("PRIDE XML - No extra peak files required.");
    } else if (MassSpecFileFormat.MZIDENTML.equals(fileFormat) || MassSpecFileFormat.MZTAB.equals(fileFormat)) {
      peakListFiles = new ArrayList<>();
      for (DataFile df : dataFile.getFileMappings()) {
        if (df.getFileType().equals(ProjectFileType.PEAK)) {
          File peakFile = fileFinder.find(df.getFile());
          peakListFiles.add(peakFile);
        }
      }
      logger.info(fileFormat + " - gathered peak files.");
    } else {
      logger.error("File format not supported : " + fileFormat);
    }
    return peakListFiles;
  }
  .
  .
  .