package uk.ac.ebi.pride.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * This class sets up a subscriber to a Redis channel.
 */
public final class SetupJedisSubscriber {
  private static final Logger logger = LoggerFactory.getLogger(SetupJedisSubscriber.class);

  /**
   * Sets up a subscriber to a Redis channel.
   * @param messageToWatchFor the success message to watch for
   * @param messageWithError the error message to wath for
   * @param messageReceivedLatch the messageReceivedLatch to decrement
   * @param messageContainer the messageContainer to parse
   * @param jedisServer the redis server host name
   * @param jedisPort the redis serer port
   * @param jedisPassword the redis server password
   * @param jedisChannel the channel to listen to
   * @return a jedisPubSub objects that is subsribed to the channel, waiting for the good/bad message to be received.
   */
  public static JedisPubSub setupSubscriber(String messageToWatchFor, String messageWithError,
                                        CountDownLatch messageReceivedLatch, ArrayList<String> messageContainer,
                                               String jedisServer, int jedisPort, String jedisPassword, String jedisChannel) {
    logger.info("Good message to watch for: " + messageToWatchFor);
    logger.info("Error message to watch for: " + messageWithError);
    final JedisPubSub jedisPubSub = new JedisPubSub() {
      public void onUnsubscribe(String channel, int subscribedChannels) {
        logger.info("Unsubscribed to JEDIS channel for " + messageToWatchFor + ".");
      }

      public void onSubscribe(String channel, int subscribedChannels) {
        logger.info("Subscribed to JEDIS channel to watch for: " + messageToWatchFor);
      }

      public void onPUnsubscribe(String pattern, int subscribedChannels) {
      }

      public void onPSubscribe(String pattern, int subscribedChannels) {
      }

      public void onPMessage(String pattern, String channel, String message) {
      }

      public void onMessage(String channel, String message) {
        if (message.equalsIgnoreCase(messageWithError)) {
          logger.info("Received error message for: " + message);
          messageContainer.add(message);
          messageReceivedLatch.countDown();
        } else if (message.equalsIgnoreCase(messageToWatchFor)) {
          logger.info("Received OK message for: " + messageToWatchFor);
          messageContainer.add(message);
          messageReceivedLatch.countDown();
        }
      }
    };
    new Thread(() -> {
      try {
        logger.info("Subscribing to JEDIS for " + messageToWatchFor);
        JedisPool pool = new JedisPool(new JedisPoolConfig(), jedisServer, jedisPort, 0, jedisPassword);
        Jedis jedis = pool.getResource();
        jedis.subscribe(jedisPubSub, jedisChannel);
        jedis.quit();
      } catch (Exception e) {
        logger.error("Exception when subscribing to JEDIS for " + messageToWatchFor, e);
      }
    }, "subscriberThread").start();
    return jedisPubSub;
  }
}
