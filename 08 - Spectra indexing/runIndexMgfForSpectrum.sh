#!/bin/sh

##### OPTIONS
# (required) the project accession
ACCESSION=""
# (optional)  To send notification message on completion
TO_NOTIFY=false


##### VARIABLES
# the name to give to the LSF job (to be extended with additional info)
JOB_NAME="post-submission-index-mgf"
# the job parameters that are going to be passed on to the job (build below)
JOB_PARAMETERS="random.number="$RANDOM
# memory limit
MEMORY_LIMIT=15000
# LSF email notification
JOB_EMAIL="${pride.report.email}"
# Log file name
LOG_FILE_NAME=""
# index step size
STEP_SIZE=500

##### FUNCTIONS
printUsage() {
    echo "Description: Index spectrum pipeline index PRIDE generated mgf files for spectra"
    echo "             This should only be run after the mgf generation pipeline (runMgfGeneration.sh)"
    echo ""
    echo "Usage: ./runIndexMgf.sh -a|--accession [-n|--notify] [-e|--email] [-s|--stepSize]"
    echo "     Example: ./runIndexMgf.sh -a PXD000542 -n"
    echo "     (required) accession     : project accession"
    echo "     (optional) notify        : Whether to notify on completion, default is false"
    echo "     (optional) email         : Email to send LSF notification"
    echo "     (optional) stepSize      : Size of the spectra documents that will be send to solr during the indexing"

}


##### PARSE the provided parameters
while [ "$1" != "" ]; do
    case $1 in
      "-a" | "--accession")
        shift
        ACCESSION=$1
        LOG_FILE_NAME="${ACCESSION}-${JOB_NAME}"
        JOB_NAME="${JOB_NAME}-${ACCESSION}"
        JOB_PARAMETERS="${JOB_PARAMETERS} project.accession=${ACCESSION}"
        ;;
      "-n" | "--notify")
        shift
        TO_NOTIFY=true
        ;;
      "-e" | "--email")
        shift
        JOB_EMAIL=$1
        ;;
      "-s" | "--stepSize")
        shift
        STEP_SIZE=$1
        ;;
    esac
    shift
done

##### CHECK the provided arguments
if [ -z ${ACCESSION} ]; then
         echo "Need to enter a project accession"
         printUsage
         exit 1
fi

##### Change directory to where the script locate
cd ${0%/*}

##### RUN it on the production queue
##### NOTE: you can change LSF group to modify the number of jobs can be run concurrently #####
bsub -M ${MEMORY_LIMIT} -R "rusage[mem=${MEMORY_LIMIT}]" -q production-rh7 -g /pride_archive_index_mgf -u ${JOB_EMAIL} -J ${JOB_NAME} ./runPipelineInJava.sh ./log/mgf_gen_ind/${LOG_FILE_NAME}.log ${MEMORY_LIMIT}m -DENVIRONMENT=${database.environment} -jar ${project.build.finalName}.jar launch-index-mgf.xml indexMgfJob to.notify=${TO_NOTIFY} ${JOB_PARAMETERS} step.size=${STEP_SIZE}
