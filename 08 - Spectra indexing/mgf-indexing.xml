<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:batch="http://www.springframework.org/schema/batch"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:bean="http://www.springframework.org/schema/beans"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/batch http://www.springframework.org/schema/batch/spring-batch.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <description>
        This job indexes project using mgf files from a submission, it requires the following input parameters:
        1. (required) project accession [project.accession]
        2. (required) notification, whether to notify message queue of the result [to.notify]
    </description>

    <batch:job id="indexMgfJob" xmlns="http://www.springframework.org/schema/batch">

        <!-- Find project file path -->
        <!--populates execution context with 'project.file.path'-->
        <batch:step id="findProjectFilePathStep" parent="parentStep" next="indexMgfStep">
            <batch:tasklet>
                <bean:bean id="findProjectFilePathTasklet" scope="step" class="uk.ac.ebi.pride.tasklet.file.FindProjectFilePathTasklet">
                    <bean:property name="projectAccession" value="#{jobParameters['project.accession']}"/>
                    <bean:property name="projectRepository" ref="projectRepository"/>
                    <bean:property name="rootPath" value="file:${pride.repo.data.base.dir}"/>
                    <bean:property name="filePathBuilder">
                        <bean:bean id="filePathBuilder" class="uk.ac.ebi.pride.archive.utils.config.FilePathBuilderPride3"/>
                    </bean:property>
                </bean:bean>
            </batch:tasklet>
        </batch:step>

        <!-- Index spectrum using its mgf files-->
        <batch:step id="indexMgfStep" parent="parentStep" next="notifyIndexMgfCompletionStep">
            <batch:tasklet>
                <bean:bean id="indexProjectTasklet" scope="step" class="uk.ac.ebi.pride.tasklet.mgf.IndexProjectMGFFilesTasklet">
                    <bean:property name="projectAccession" value="#{jobParameters['project.accession']}"/>
                    <bean:property name="mgfFileDirectory" value="file:#{jobExecutionContext['project.file.path']}/internal"/>
                    <bean:property name="assayRepository" ref="assayRepository"/>
                    <bean:property name="projectRepository" ref="projectRepository"/>
                    <bean:property name="projectFileRepository" ref="projectFileRepository"/>
                    <bean:property name="mgfIndexer" >
                    <bean:bean id="spectrumMGFIndexer" class="uk.ac.ebi.pride.mgf.SpectrumMGFIndexer">
                        <bean:constructor-arg name="projectSpectraIndexer">
                            <bean:bean class="uk.ac.ebi.pride.spectrumindex.search.indexer.ProjectSpectraIndexer">
                                <bean:constructor-arg name="spectrumIndexService" ref="spectrumIndexService"/>
                                <bean:constructor-arg name="indexingSizeStep" value="#{jobParameters['step.size']}"/>
                            </bean:bean>
                        </bean:constructor-arg>
                    </bean:bean>
                    </bean:property>
                </bean:bean>
            </batch:tasklet>
        </batch:step>

        <!-- Notify index project completion -->
        <batch:step id="notifyIndexMgfCompletionStep" parent="parentStep">
            <batch:tasklet>
                <bean:bean id="messageNotificationTasklet" scope="step" class="uk.ac.ebi.pride.tasklet.message.MessageNotificationTasklet">
                    <bean:property name="queue" value="${message.mgf.index.completion.queue}"/>
                    <bean:property name="payload">
                        <bean:bean id="payload"
                                   class="uk.ac.ebi.pride.integration.message.model.impl.IndexCompletionPayload">
                            <bean:constructor-arg name="accession" value="#{jobParameters['project.accession']}"/>
                            <bean:constructor-arg name="indexType" value="SPECTRUM"/>
                        </bean:bean>
                    </bean:property>
                    <bean:property name="redisMessageNotifier" ref="redisMessageNotifier"/>
                    <bean:property name="toNotify" value="#{jobParameters['to.notify']}"/>
                </bean:bean>
            </batch:tasklet>
        </batch:step>

        <batch:listeners>
            <batch:listener ref="jobListener"/>
        </batch:listeners>
    </batch:job>

    <batch:step id="parentStep" abstract="true">
        <batch:listeners>
            <batch:listener ref="executionContextPromotionListener"/>
            <batch:listener ref="throwablePromotionListener"/>
            <batch:listener ref="stepExecutionPeriodListener"/>
        </batch:listeners>
    </batch:step>

    <!--listener for promoting properties in step execution context to job execution context-->
    <bean:bean id="executionContextPromotionListener" class="org.springframework.batch.core.listener.ExecutionContextPromotionListener">
        <bean:property name="keys">
            <bean:array>
                <bean:value>project.file.path</bean:value>    <!-- added by FindProjectFilePathTasklet -->
            </bean:array>
        </bean:property>
        <bean:property name="strict" value="false"/>
    </bean:bean>

    <!--load property files-->
    <context:property-placeholder location="classpath:prop/common.properties, classpath:prop/message.properties"
                                  ignore-unresolvable="true" order="1"/>

</beans>
