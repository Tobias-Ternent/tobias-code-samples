package uk.ac.ebi.pride.tasklet.mgf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;
import uk.ac.ebi.pride.archive.dataprovider.file.ProjectFileSource;
import uk.ac.ebi.pride.archive.repo.assay.Assay;
import uk.ac.ebi.pride.archive.repo.assay.AssayRepository;
import uk.ac.ebi.pride.archive.repo.file.ProjectFile;
import uk.ac.ebi.pride.archive.repo.file.ProjectFileRepository;
import uk.ac.ebi.pride.archive.repo.project.Project;
import uk.ac.ebi.pride.archive.repo.project.ProjectRepository;
import uk.ac.ebi.pride.mgf.MGFIndexer;
import uk.ac.ebi.pride.tasklet.util.AbstractTasklet;
import uk.ac.ebi.pride.util.Constant;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Tasklet for indexing project mgf files.
 */
public class IndexProjectMGFFilesTasklet extends AbstractTasklet {
    public static final Logger logger = LoggerFactory.getLogger(IndexProjectMGFFilesTasklet.class);

    private MGFIndexer mgfIndexer;
    private String projectAccession;
    private Resource mgfFileDirectory;
    private AssayRepository assayRepository;
    private ProjectRepository projectRepository;
    private ProjectFileRepository projectFileRepository;
    private Integer stepSize = 500;

    /**
     * Executes the tasklet.
     * @param contribution the step's contribution
     * @param chunkContext the step's context
     * @return finished status
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) {
        try {
            File mgfDir = mgfFileDirectory.getFile();
            Assert.state(mgfDir.isDirectory() && mgfDir.exists(), "mgf directory must be a valid directory " + mgfDir.getAbsolutePath());
            logger.info("Indexing project mgf file for project: {}", projectAccession);
            final Project project = projectRepository.findByAccession(projectAccession);
            indexMGFFiles(mgfDir, project);
        } catch (IOException e) {
            logger.error("Failed for indexing mgf files for project: {}", projectAccession, e);
            deleteProjectFromIndex();
        }
        return RepeatStatus.FINISHED;
    }

    /**
     * Indexes an MGF file.
     * @param mgfDir the directory where the mgf file is in
     * @param project the project being indexed
     * @throws IOException problems reading the mgf file
     */
    private void indexMGFFiles(File mgfDir, Project project) throws IOException {
        final List<ProjectFile> projectFiles = projectFileRepository.findAllByProjectId(project.getId());
        for (ProjectFile projectFile : projectFiles) {
            if (isGeneratedMgfFile(projectFile)) {
                logger.info("Indexing project mgf file: {}", projectFile.getFileName());
                final File mgfFile = getMGFFile(mgfDir, projectFile);
                indexMGFFile(projectFile, mgfFile);
            }
        }
    }

    /**
     * Delete entries from the spectra index for the project accession.
     */
    private void deleteProjectFromIndex() {
        mgfIndexer.delete(projectAccession);
    }

    /**
     * Works it if a project file is a generated mgf ile or not.
     * @param projectFile the project file that may be a generated mgf file
     * @return true for being a generated mgf file, false otherwise
     */
    private boolean isGeneratedMgfFile(ProjectFile projectFile) {
        return projectFile.getFileSource().equals(ProjectFileSource.GENERATED) &&
            projectFile.getFileName().toLowerCase().endsWith(Constant.PRIDE_MGF_FILE_EXTENSION + "." + Constant.GZIP_FILE_EXTENSION);
    }

    /**
     * Indexes an mgf file
     */
    private void indexMGFFile(ProjectFile projectFile, File mgfFile) {
        final Assay assay = assayRepository.findOne(projectFile.getAssayId());
        final String assayAccession = assay.getAccession();
        mgfIndexer.index(projectAccession, assayAccession, mgfFile);
    }

    /**
     * Parse an mgf file
     */
    private File getMGFFile(File mgfDir, ProjectFile projectFile) throws IOException {
        String fileName = projectFile.getFileName();
        fileName = fileName.substring(0, fileName.length() - (Constant.GZIP_FILE_EXTENSION.length() + 1));
        return new File(mgfDir.getAbsolutePath() + Constant.FILE_SEPARATOR + fileName);
    }

    /**
     * Sets the tasklet's variables before execution.
     * @throws Exception Any failed asserts ensuring variables are set properly.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(mgfIndexer, "mgf indexer cannot be null");
        Assert.notNull(projectAccession, "Project accession cannot be null");
        Assert.notNull(mgfFileDirectory, "mgf file directory cannot be null");
        Assert.notNull(assayRepository, "Assay repository cannot be null");
        Assert.notNull(projectRepository, "Project repository cannot be null");
        Assert.notNull(projectFileRepository, "Project file repository cannot be null");
        Assert.notNull(stepSize, "stepSize cannot be null");
    }

    public void setMgfIndexer(MGFIndexer mgfIndexer) {
        this.mgfIndexer = mgfIndexer;
    }

    public void setProjectAccession(String projectAccession) {
        this.projectAccession = projectAccession;
    }

    public void setMgfFileDirectory(Resource mgfFileDirectory) {
        this.mgfFileDirectory = mgfFileDirectory;
    }

    public void setAssayRepository(AssayRepository assayRepository) {
        this.assayRepository = assayRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void setProjectFileRepository(ProjectFileRepository projectFileRepository) {
        this.projectFileRepository = projectFileRepository;
    }
}
