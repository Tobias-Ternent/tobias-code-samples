# Code samples #

This repository contains sample code, from PRIDE-related projects. It is divided up into separate directories, one topic per directory. See below for a list of all directories and a short title of the contents. Samples range from exerts from classes, and a couple are entire libraries. For Java samples, the primary language used, the code will not necessarily compile even for entire programs, because mandatory Maven profiles are missing.

* 01 - Rate limiter
* 02 - DOI WS
* 03 - Schema check
* 04 - Starting validation
* 05 - File validation
* 06 - User Profile
* 07 - Track hub (Python)
* 08 - Spectra indexing
* 09 - PSM indexing
* 10 - Queue notification

### Who do I talk to? ###

If you want more information, or in-depth explanations, please contact the author: tobias.ternent@gmail.com