  .
  .
  .
  /**
   * Publishes a success or failure message to the specified Redis channel according to the credentials used.
   * @param jedisServer the Redis server name.
   * @param jedisPort the Redis server port.
   * @param jedisPassword the Redis password
   * @param assayChannel the Redis channel to post a message to.
   * @param message the message content.
   */
  public static void notifyRedisChannel(String jedisServer, int jedisPort, String jedisPassword, String assayChannel, String message) {
      log.info("Connecting to REDIS channel:" + assayChannel);
      JedisPool pool = new JedisPool(new JedisPoolConfig(), jedisServer, jedisPort, 0, jedisPassword);
      Jedis jedis = pool.getResource();
      log.info("Publishing message to Redis: " + message);
      jedis.publish(assayChannel, message);
      log.info("Published message to Redis, closing connection");
      jedis.quit();
  }
  .
  .
  .