.
.
.
 /**
   * This method validates an input assay file.
   *
   * @param assayFile the input assay file.
   * @return an array of objects[2]: a Report object and an AssayFileSummary, respectively.
   */
  private static ValidationResult validateAssayFile(File assayFile, FileType type, List<File> dataAccessControllerFiles) {
    File tempFile = createNewTempFile(assayFile);
    List<File> tempDataAccessControllerFiles = new ArrayList<>();
    boolean badtempDataAccessControllerFiles = true;
    if (CollectionUtils.isNotEmpty(dataAccessControllerFiles)) {
      for (File dataAccessControllerFile : dataAccessControllerFiles) {
        File tempDataAccessControllerFile = createNewTempFile(dataAccessControllerFile);
        if (tempDataAccessControllerFile!=null && 0<tempDataAccessControllerFile.length()) {
          tempDataAccessControllerFiles.add(tempDataAccessControllerFile);
        }
      }
      badtempDataAccessControllerFiles = CollectionUtils.isEmpty(tempDataAccessControllerFiles) ||
          tempDataAccessControllerFiles.size()!=dataAccessControllerFiles.size();
    }
    log.info("Validating assay file: " + assayFile.getAbsolutePath());
    log.info("From temp file: " + tempFile.getAbsolutePath());
    AssayFileSummary assayFileSummary = new AssayFileSummary();
    Report report = new Report();
    try {
      final AssayFileController assayFileController;
      switch(type) {
        case MZID :
          assayFileController = new MzIdentMLControllerImpl(tempFile!=null ? tempFile : assayFile);
          assayFileController.addMSController(badtempDataAccessControllerFiles ? dataAccessControllerFiles : tempDataAccessControllerFiles);
          break;
        case PRIDEXML :
          assayFileController = new PrideXmlControllerImpl(tempFile!=null ? tempFile : assayFile);
          break;
        case MZTAB : assayFileController = new MzTabControllerImpl(tempFile!=null ? tempFile : assayFile);
          assayFileController.addMSController(badtempDataAccessControllerFiles ? dataAccessControllerFiles : tempDataAccessControllerFiles);
          break;
        default : log.error("Unrecognized assay fle type: " + type);
          assayFileController = new MzIdentMLControllerImpl(tempFile);
          break;
      }
      final int NUMBER_OF_CHECKS = 10;
      List<Boolean> randomChecks = new ArrayList<>();
      IntStream.range(1, NUMBER_OF_CHECKS).sequential().forEach(i -> randomChecks.add(assayFileController.checkRandomSpectraByDeltaMassThreshold(NUMBER_OF_CHECKS, 4.0)));
      int checkFalseCounts = 0;
      for (Boolean check : randomChecks) {
        if (!check) {
          checkFalseCounts++;
        }
      }
      assayFileSummary.setDeltaMzErrorRate(new BigDecimal(((double) checkFalseCounts / (NUMBER_OF_CHECKS*NUMBER_OF_CHECKS))).setScale(2, RoundingMode.HALF_UP).doubleValue());
      report.setFileName(assayFile.getAbsolutePath());
      assayFileSummary.setNumberOfIdentifiedSpectra(assayFileController.getNumberOfIdentifiedSpectra());
      assayFileSummary.setNumberOfPeptides(assayFileController.getNumberOfPeptides());
      assayFileSummary.setNumberOfProteins(assayFileController.getNumberOfProteins());
      assayFileSummary.setNumberofMissingSpectra(assayFileController.getNumberOfMissingSpectra());
      assayFileSummary.setNumberOfSpectra(assayFileController.getNumberOfSpectra());
      if (assayFileSummary.getNumberofMissingSpectra()<1) {
        validateProteinsAndPeptides(assayFile, assayFileSummary, assayFileController);
      } else {
        String message = "Missing spectra are present";
        log.error(message);
        report.setStatusError(message);
      }
      scanForGeneralMetadata(assayFileController, assayFileSummary);
      scanForInstrument(assayFileController, assayFileSummary);
      scanForSoftware(assayFileController, assayFileSummary);
      scanForSearchDetails(assayFileController, assayFileSummary);
      switch(type) {
        case MZID :
        case MZTAB :
          scanRefIdControllerpecificDetails((ReferencedIdentificationController) assayFileController, dataAccessControllerFiles, assayFileSummary);
          break;
        default : // do nothing
          break;
      }
      if (StringUtils.isEmpty(report.getStatus())) {
        report.setStatusOK();
      }
    } catch (NullPointerException e) {
      log.error("Null pointer Exception when scanning assay file", e);
      report.setStatusError(e.getMessage());
    } finally {
      if (tempFile!=null) {
        deleteTempFile(tempFile);
      }
      if (CollectionUtils.isNotEmpty(tempDataAccessControllerFiles)) {
        for (File dataAccessControllerFile : tempDataAccessControllerFiles) {
          if (dataAccessControllerFile != null) {
            deleteTempFile(dataAccessControllerFile);
          }
        }
      }
    }
    return new ValidationResult(assayFileSummary, report);
  }
  .
  .
  .
  /**
   * Validates across proteins and peptides for a given assay file
   * @param assayFile the assay file (e.g. .mzid file)
   * @param assayFileSummary the assay file summary
   * @param assayFileController the assay file controller (e.g. for mzIdentML etc).
   */
  private static void validateProteinsAndPeptides(File assayFile, AssayFileSummary assayFileSummary, AssayFileController assayFileController) throws NullPointerException {
    Set<String> uniquePeptides = new HashSet<>();
    Set<CvParam> ptms = new HashSet<>();
    for (Comparable proteinId : assayFileController.getProteinIds()) {
      for (Peptide peptide : assayFileController.getProteinById(proteinId).getPeptides()) {
        uniquePeptides.add(peptide.getSequence());
        for (Modification modification : peptide.getModifications()) {
          for (CvParam cvParam : modification.getCvParams()) {
            if (StringUtils.isEmpty(cvParam.getCvLookupID())|| StringUtils.isEmpty(cvParam.getAccession()) || StringUtils.isEmpty(cvParam.getName())) {
              String message = "A PTM CV Param's ontology, accession, or name is not defined properly: " + cvParam.toString() + " in file: " +  assayFile.getPath();
              log.error(message);
              throw new NullPointerException(message);
            }
            if (cvParam.getCvLookupID().equalsIgnoreCase(Constant.PSI_MOD) || cvParam.getCvLookupID().equalsIgnoreCase(Constant.UNIMOD)) {
              ptms.add(cvParam);
            }
          }
        }
      }
    }
    List<Boolean> matches = new ArrayList<>();
    matches.add(true);
    IntStream.range(1, (assayFileController.getNumberOfPeptides()<100 ? assayFileController.getNumberOfPeptides() : 100)).sequential().forEach(i -> {
      Peptide peptide = assayFileController.getProteinById(assayFileController.getProteinIds().stream().findAny().get()).getPeptides().stream().findAny().get();
      if (peptide.getFragmentation() != null && peptide.getFragmentation().size() > 0) {
        if (!matchingFragmentIons(peptide.getFragmentation(), peptide.getSpectrum())) {
          matches.add(false);
        }
      }
    });
    assayFileSummary.addPtms(DataConversionUtil.convertAssayPTMs(ptms));
    assayFileSummary.setSpectrumMatchFragmentIons(matches.size() <= 1);
    assayFileSummary.setNumberOfUniquePeptides(uniquePeptides.size());
  }
  .
  .
  .
 /**
   * This method checks to see if the fragment ions match the spectrum.
   *
   * @param fragmentIons the fragment ions.
   * @param spectrum the spectrum.
   * @return true if they match, false otherwise.
   */
  private static boolean matchingFragmentIons(List<FragmentIon> fragmentIons, Spectrum spectrum) {
    double[][] massIntensityMap = spectrum.getMassIntensityMap();
    for (FragmentIon fragmentIon : fragmentIons) {
      double intensity = fragmentIon.getIntensity();
      double mz = fragmentIon.getMz();
      boolean matched = false;
      for (double[] massIntensity : massIntensityMap) {
        if (massIntensity[0] == mz && massIntensity[1] == intensity) {
          matched = true;
          break;
        }
      }
      if (!matched) {
        return false;
      }
    }
    return true;
  }
  .
  .
  .
  /**
   * This method scans for general metadata.
   *
   * @param dataAccessController the input controller to read over.
   * @param assayFileSummary the assay file summary to output results to.
   */
  private static void scanForGeneralMetadata(DataAccessController dataAccessController, AssayFileSummary assayFileSummary) {
    log.info("Started scanning for general metadata.");
    String title = dataAccessController.getExperimentMetaData().getName();
    assayFileSummary.setName(StringUtils.isEmpty(title) || title.contains("no assay title provided") ?
        dataAccessController.getName() :
        title);
    assayFileSummary.setShortLabel(StringUtils.isEmpty(dataAccessController.getExperimentMetaData().getShortLabel()) ?
        "" :
        dataAccessController.getExperimentMetaData().getShortLabel());
    assayFileSummary.addContacts(DataConversionUtil.convertContact(dataAccessController.getExperimentMetaData().getPersons()));
    ParamGroup additional = dataAccessController.getExperimentMetaData().getAdditional();
    assayFileSummary.addCvParams(DataConversionUtil.convertAssayGroupCvParams(additional));
    assayFileSummary.addUserParams(DataConversionUtil.convertAssayGroupUserParams(additional));
    log.info("Finished scanning for general metadata.");
  }
  .
  .
  .
  /**
   * This method scans for instruments metadata.
   *
   * @param dataAccessController the input controller to read over.
   * @param assayFileSummary the assay file summary to output results to.
   */
  private static void scanForInstrument(DataAccessController dataAccessController, AssayFileSummary assayFileSummary) {
    log.info("Started scanning for instruments");
    Set<Instrument> instruments = new HashSet<>();
    // check to see if we have instrument configurations in the result file to scan, this isn't always present
    MzGraphMetaData mzGraphMetaData = null;
    try {
      mzGraphMetaData = dataAccessController.getMzGraphMetaData();
    } catch (Exception e) {
      log.error("Exception while getting mzgraph instrument data." + e);
    }
    if (mzGraphMetaData != null) {
      Collection<InstrumentConfiguration> instrumentConfigurations = dataAccessController.getMzGraphMetaData().getInstrumentConfigurations();
      for (InstrumentConfiguration instrumentConfiguration : instrumentConfigurations) {
        Instrument instrument = new Instrument();
        // set instrument cv param
        uk.ac.ebi.pride.archive.repo.param.CvParam cvParam = new uk.ac.ebi.pride.archive.repo.param.CvParam();
        cvParam.setCvLabel(Constant.MS);
        cvParam.setName(Utility.MS_INSTRUMENT_MODEL_NAME);
        cvParam.setAccession(Utility.MS_INSTRUMENT_MODEL_AC);
        instrument.setCvParam(cvParam);
        instrument.setValue(instrumentConfiguration.getId());
        // build instrument components
        instrument.setSources(new ArrayList<>());
        instrument.setAnalyzers(new ArrayList<>());
        instrument.setDetectors(new ArrayList<>());
        int orderIndex = 1;
        // source
        for (InstrumentComponent source : instrumentConfiguration.getSource()) {
          if (source!=null) {
            SourceInstrumentComponent sourceInstrumentComponent = new SourceInstrumentComponent();
            sourceInstrumentComponent.setInstrument(instrument);
            sourceInstrumentComponent.setOrder(orderIndex++);
            sourceInstrumentComponent.setInstrumentComponentCvParams(DataConversionUtil.convertInstrumentComponentCvParam(sourceInstrumentComponent, source.getCvParams()));
            sourceInstrumentComponent.setInstrumentComponentUserParams(DataConversionUtil.convertInstrumentComponentUserParam(sourceInstrumentComponent, source.getUserParams()));
            instrument.getSources().add(sourceInstrumentComponent);
          }
        }
        // analyzer
        for (InstrumentComponent  analyzer: instrumentConfiguration.getAnalyzer()) {
          if (analyzer!=null) {
            AnalyzerInstrumentComponent analyzerInstrumentComponent = new AnalyzerInstrumentComponent();
            analyzerInstrumentComponent.setInstrument(instrument);
            analyzerInstrumentComponent.setOrder(orderIndex++);
            analyzerInstrumentComponent.setInstrumentComponentCvParams(DataConversionUtil.convertInstrumentComponentCvParam(analyzerInstrumentComponent, analyzer.getCvParams()));
            analyzerInstrumentComponent.setInstrumentComponentUserParams(DataConversionUtil.convertInstrumentComponentUserParam(analyzerInstrumentComponent, analyzer.getUserParams()));
            instrument.getAnalyzers().add(analyzerInstrumentComponent);
          }
        }
        // detector
        for (InstrumentComponent detector : instrumentConfiguration.getDetector()) {
          if (detector!=null) {
            DetectorInstrumentComponent detectorInstrumentComponent = new DetectorInstrumentComponent();
            detectorInstrumentComponent.setInstrument(instrument);
            detectorInstrumentComponent.setOrder(orderIndex++);
            detectorInstrumentComponent.setInstrumentComponentCvParams(DataConversionUtil.convertInstrumentComponentCvParam(detectorInstrumentComponent, detector.getCvParams()));
            detectorInstrumentComponent.setInstrumentComponentUserParams(DataConversionUtil.convertInstrumentComponentUserParam(detectorInstrumentComponent, detector.getUserParams()));
            instrument.getDetectors().add(detectorInstrumentComponent);
          }
        }
        instruments.add(instrument); //store instrument
      }
    } // else do nothing
    assayFileSummary.addInstruments(instruments);
    log.info("Finished scanning for instruments");
  }
  .
  .
  .
  /**
   * This method scans for software metadata.
   *
   * @param dataAccessController the input controller to read over.
   * @param assayFileSummary the assay file summary to output results to.
   */
  private static void scanForSoftware(DataAccessController dataAccessController, AssayFileSummary assayFileSummary) {
    log.info("Started scanning for software");
    ExperimentMetaData experimentMetaData = dataAccessController.getExperimentMetaData();
    Set<Software> softwares = new HashSet<>(experimentMetaData.getSoftwares());
    Set<uk.ac.ebi.pride.archive.repo.assay.software.Software> softwareSet = new HashSet<>(DataConversionUtil.convertSoftware(softwares));
    assayFileSummary.addSoftwares(softwareSet);
    log.info("Finished scanning for software");
  }
  .
  .
  .
  /**
   * This method scans for search details metadata.
   *
   * @param dataAccessController the input controller to read over.
   * @param assayFileSummary the assay file summary to output results to.
   */
  private static void scanForSearchDetails(DataAccessController dataAccessController, AssayFileSummary assayFileSummary) {
    log.info("Started scanning for search details");
    // protein group
    boolean proteinGroupPresent = dataAccessController.hasProteinAmbiguityGroup();
    assayFileSummary.setProteinGroupPresent(proteinGroupPresent);
    Collection<Comparable> proteinIds = dataAccessController.getProteinIds();
    if (proteinIds != null && !proteinIds.isEmpty()) {
      Comparable firstProteinId = proteinIds.iterator().next();
      // protein accession
      String accession = dataAccessController.getProteinAccession(firstProteinId);
      assayFileSummary.setExampleProteinAccession(accession);
      // search database
      SearchDataBase searchDatabase = dataAccessController.getSearchDatabase(firstProteinId);
      if (searchDatabase != null) {
        assayFileSummary.setSearchDatabase(searchDatabase.getName());
      }
    }
    log.info("Finished scanning for search details");
  }
  .
  .
  .
  /**
   * This method scans for ReferencedIdentificationController-specific metadata.
   *
   * @param referencedIdentificationController the input controller to read over.
   * @param peakFiles the input related peak files.
   * @param assayFileSummary the assay file summary to output results to.
   */
  private static void scanRefIdControllerpecificDetails(ReferencedIdentificationController referencedIdentificationController, List<File> peakFiles, AssayFileSummary assayFileSummary) {
    log.info("Started scanning for mzid- or mztab-specific details");
    Set<PeakFileSummary> peakFileSummaries = new HashSet<>();
    List<String> peakFileNames = new ArrayList<>();
    for (File peakFile : peakFiles) {
      peakFileNames.add(peakFile.getName());
      String extension = FilenameUtils.getExtension(peakFile.getAbsolutePath());
      if (MassSpecFileFormat.MZML.toString().equalsIgnoreCase(extension)) {
        log.info("MzML summary: " + getMzMLSummary(peakFile, assayFileSummary));
        break;
      }
    }
    List<SpectraData> spectraDataFiles = referencedIdentificationController.getSpectraDataFiles();
    for (SpectraData spectraDataFile : spectraDataFiles) {
      String location = spectraDataFile.getLocation();
      String realFileName = FileUtil.getRealFileName(location);
      Integer numberOfSpectrabySpectraData = referencedIdentificationController.getNumberOfSpectrabySpectraData(spectraDataFile);
      peakFileSummaries.add(new PeakFileSummary(realFileName, !peakFileNames.contains(realFileName), numberOfSpectrabySpectraData));
    }
    assayFileSummary.addPeakFileSummaries(peakFileSummaries);
    log.info("Finished scanning for ReferencedIdentificationController-specific details");
  }
  .
  .
  .
  /**
   * This method checks if a mapped mzML file has chromatograms or not.
   * @param mappedFile the input mzML file.
   * @param assayFileSummary the assay file summary to output the result to.
   * @return true if a mzML has chromatograms, false otherwise.
   */
  private static boolean getMzMLSummary(File mappedFile, AssayFileSummary assayFileSummary) {
    log.info("Getting mzml summary.");
    MzMLControllerImpl mzMLController = null;
    boolean result = false;
    try {
      mzMLController = new MzMLControllerImpl(mappedFile);
      if (mzMLController.hasChromatogram()) {
        assayFileSummary.setChromatogram(true);
        mzMLController.close();
        result = true;
      }
    } finally {
      if (mzMLController != null) {
        log.info("Finished getting mzml summary.");
        mzMLController.close();
      }
    }
    return result;
  }
  .
  .
  .