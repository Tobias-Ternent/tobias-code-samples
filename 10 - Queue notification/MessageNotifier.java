package uk.ac.ebi.pride.integration.message.service;

/**
 * MessageNotifier is an interface to sending message notification
 * Q: represents the address where the message will be sent
 */
public interface MessageNotifier<Q> {

    public <T> void sendNotification(Q queue, T payload, Class<T> payloadClass);
}
