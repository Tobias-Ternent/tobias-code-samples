package uk.ac.ebi.pride.tasklet.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.util.Assert;
import uk.ac.ebi.pride.integration.message.service.impl.RedisMessageNotifier;
import uk.ac.ebi.pride.tasklet.util.AbstractTasklet;

/**
 * Notifies the archive-integration service via Redis.
 */
public class MessageNotificationTasklet<T> extends AbstractTasklet {

    public static final Logger logger = LoggerFactory.getLogger(MessageNotificationTasklet.class);

    private String queue;

    private T payload;

    private RedisMessageNotifier redisMessageNotifier;

    private boolean toNotify;

    /**
     * Executes the tasklet to send a notitication to the Redis queue.
     * @param contribution the step's contribution
     * @param chunkContext the step's context
     * @return finished status
     * @throws Exception problems notifying the archive-integration service
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        if (toNotify) {
            logger.info("Sending notification message: " + payload.toString());
            redisMessageNotifier.sendNotification(queue, payload, (Class<T>) payload.getClass());
        } else {
            logger.info("Notification message was not sent");
        }
        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the tasklet's properties before execution.
     * @throws Exception properties have not been setup properly
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(queue, "Messaging queue cannot be null");
        Assert.notNull(payload, "Message payload cannot be null");
        Assert.notNull(redisMessageNotifier, "Redis message notifier cannot be null");
        Assert.notNull(toNotify, "To notify parameter cannot be null");
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public void setRedisMessageNotifier(RedisMessageNotifier redisMessageNotifier) {
        this.redisMessageNotifier = redisMessageNotifier;
    }

    public void setToNotify(boolean toNotify) {
        this.toNotify = toNotify;
    }
}
